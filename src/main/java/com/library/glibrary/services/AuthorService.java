package com.library.glibrary.services;

import com.library.glibrary.dao.AuthorRepository;
import com.library.glibrary.entities.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    public void save(Author author){
        authorRepository.save(author);
    };

    public List<Author> findAll(){
        return authorRepository.findAll();
    };

    public void delete(Author author){
        authorRepository.delete(author);
    }
}
