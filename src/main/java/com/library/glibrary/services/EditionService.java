package com.library.glibrary.services;

import com.library.glibrary.dao.AuthorRepository;
import com.library.glibrary.dao.EditionRepository;
import com.library.glibrary.entities.Author;
import com.library.glibrary.entities.Edition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EditionService {

    @Autowired
    private EditionRepository editionRepository;

    public void save(Edition edition){
        editionRepository.save(edition);
    };

    public List<Edition> findAll(){
        return editionRepository.findAll();
    };

}
