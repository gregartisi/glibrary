package com.library.glibrary.services;

import com.library.glibrary.dao.BookRepository;
import com.library.glibrary.dao.EditionRepository;
import com.library.glibrary.entities.Book;
import com.library.glibrary.entities.Edition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public void save(Book book){
        bookRepository.save(book);
    };

    public List<Book> findAll(){
        return bookRepository.findAll();
    };
}
