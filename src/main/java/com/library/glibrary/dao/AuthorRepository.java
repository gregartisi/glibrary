package com.library.glibrary.dao;

import com.library.glibrary.entities.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author,Long>{



}
