package com.library.glibrary.dao;

import com.library.glibrary.entities.Author;
import com.library.glibrary.entities.Edition;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EditionRepository extends JpaRepository<Edition,Long> {
}
