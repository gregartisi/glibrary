package com.library.glibrary.entities;

import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity @ToString
public class Book {


    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String title;
    private String codeISBN;
    private String codeLibrary;
    private String description;

    @ManyToOne
    private Author author;

    @ManyToOne
    private Edition edition;

    private Date dateEdition;

    @ManyToMany
    @JoinTable(
            name = "Book_IndexExpression",
            joinColumns = { @JoinColumn(name = "book_id") },
            inverseJoinColumns = { @JoinColumn(name = "IndexExpression_id") }
    )
    private List<IndexExpression> indexation;


    public Book() {

    }

    public Book(String title, String codeISBN, String codeLibrary, String description, Author author, Edition edition, Date dateEdition, List<IndexExpression> indexation) {
        this.title = title;
        this.codeISBN = codeISBN;
        this.codeLibrary = codeLibrary;
        this.description = description;
        this.author = author;
        this.edition = edition;
        this.dateEdition = dateEdition;
        this.indexation = indexation;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return this.title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getCodeISBN() {
        return this.codeISBN;
    }


    public void setCodeISBN(String codeISBN) {
        this.codeISBN = codeISBN;
    }



    public String getCodeLibrary() {
        return this.codeLibrary;
    }


    public void setCodeLibrary(String codeLibrary) {
        this.codeLibrary = codeLibrary;
    }


    public String getDescription() {
        return this.description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public Author getAuthor() {
        return this.author;
    }


    public void setAuthor(Author author) {
        this.author = author;
    }


    public Edition getEdition() {
        return this.edition;
    }


    public void setEdition(Edition edition) {
        this.edition = edition;
    }


    public Date getDateEdition() {
        return this.dateEdition;
    }


    public void setDateEdition(Date dateEdition) {
        this.dateEdition = dateEdition;
    }


    public List<IndexExpression> getIndexation() {
        return this.indexation;
    }


}
