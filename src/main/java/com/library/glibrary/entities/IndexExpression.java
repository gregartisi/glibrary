package com.library.glibrary.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class IndexExpression {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String expression;


    /**
     * get field @Id
     @GeneratedValue(strategy= GenerationType.AUTO)

      *
      * @return id @Id
     @GeneratedValue(strategy= GenerationType.AUTO)

     */
    public Long getId() {
        return this.id;
    }


    /**
     * get field
     *
     * @return expression
     */
    public String getExpression() {
        return this.expression;
    }

    /**
     * set field
     *
     * @param expression
     */
    public void setExpression(String expression) {
        this.expression = expression;
    }
}
