package com.library.glibrary;

import com.library.glibrary.dao.AuthorRepository;
import com.library.glibrary.dao.BookRepository;
import com.library.glibrary.dao.EditionRepository;
import com.library.glibrary.entities.Author;
import com.library.glibrary.entities.Book;
import com.library.glibrary.entities.Edition;
import com.library.glibrary.entities.IndexExpression;
import com.library.glibrary.services.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Calendar;
import java.util.Date;

@SpringBootApplication
public class GlibraryApplication implements CommandLineRunner {

	@Autowired
	private AuthorService authorService;

	public static void main(String[] args) {
		SpringApplication.run(GlibraryApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		//author
		Author author = new Author("King", "Stephen");
		authorService.save(author);
		Author author1 = new Author("Tolkien", "John Ronald Reuel");
		authorService.save(author1);
		Author author2 = new Author("Austen", "Jane");
		authorService.save(author2);

		authorService.findAll().forEach(a->System.out.println(a.toString()));

	}
}
