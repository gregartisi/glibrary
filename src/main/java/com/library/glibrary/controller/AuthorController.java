package com.library.glibrary.controller;

import com.library.glibrary.entities.Author;
import com.library.glibrary.services.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AuthorController {

    @Autowired
    private AuthorService authorService ;


    @GetMapping(value = "/authors")
    public List<Author> getAuthors() {
        return authorService.findAll();
    }

    @PostMapping(value="/authors")
    public void saveAuthor(@RequestBody Author author){
        authorService.save(author);

    }

    @PutMapping(value="/authors/update/{id}")
    public void saveAuthor(@PathVariable("id") int authorId,@RequestBody Author author){
        authorService.save(author);

    }

    @DeleteMapping(value = "/authors/delete/{id}")
    public void delete(@RequestBody Author author) {
        authorService.delete(author);
    }
}
